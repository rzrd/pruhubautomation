package configs;

public class ElementsInspect {
//	login Page
	public static final String loginPageUrl = "https://pruhub-sit.pru.intranet.asia/pruhub-2-pmn/#/login";
	public static final String homepageUrl = "https://pruhub-sit.pru.intranet.asia/pruhub-2-pmn/#/home";
	
	public static final String username = "//input[contains(@placeholder,'Username')]";
	public static final String password = "//input[contains(@placeholder,'Password')]";
	public static final String loginButton = "//span[@class='button-inner'][contains(.,'Login')]";
	public static final String logoutButton = "//span[contains(@class,'logout')]";
	public static final String msgAlert = "//h3[contains(@id,'alert-subhdr-0')]";
	public static final String acceptAlert = "//div[@class='alert-button-group']";
	
//	general
	public static final String policyNumber = "//span[@class='last-decision'][contains(.,'Admisson ID : QKI300')]";
	public static final String arrowBack = "//ion-icon[@name='arrow-back']";
	public static final String toastMsg = "//span[contains(.,'Close')]";
	
//	open workbasket
	public static final String pmnWrokbasketUrl = "https://pruhub-sit.pru.intranet.asia/pruhub-2-pmn/#/pmn-workbasket";
	public static final String openMenu = "//ion-icon[@aria-label='add circle']";
	public static final String pmnWorkbasket = "(//div[contains(@class,'input-wrapper')])[2]";
	public static final String selectTask = "(//ion-col[contains(@class,'task-name padding-top-20 col')])[3]";
	public static final String titleTask = "(//font[@ng-reflect-klass='font-size-16'])";
	
//	PMN Workbasket Detail
	
	// Decision and comment Tab
	public static final String decisionCommentUrl = "https://pruhub-sit.pru.intranet.asia/pruhub-2-pmn/#/pmn-decision";
	public static final String releaseLmlUrl = "https://pruhub-sit.pru.intranet.asia/pruhub-2-pmn/#/pmn-decision-release-lml";
	public static final String declineLogUrl = "https://pruhub-sit.pru.intranet.asia/pruhub-2-pmn/#/pmn-decision-approve-log";
	public static final String approveGopUrl = "https://pruhub-sit.pru.intranet.asia/pruhub-2-pmn/#/pmn-decision-approve-gop";
	public static final String rejectLogUrl = declineLogUrl;
	public static final String rejectGopUrl = approveGopUrl;
	public static final String decisionCommentTab = "(//a[@ng-reflect-ng-class='[object Object]'])[3]";
	public static final String searchBarDiagnoseField = "//input[@class='searchbar-input']";
	public static final String clickSearchResultDiag ="/html/body/ion-app/ng-component/ion-nav/page-pmn-decision/ion-content/div[2]/ion-grid[1]/div/div[1]/ion-grid/div[2]/div/ion-row[6]/ion-col[2]/ion-list/ion-item[1]/div[1]/div";
	public static final String diagnosisRemarksField = "(//input[contains(@class,'text-input text-input-ios')])[11]";
	public static final String estimatedAmountField = "(//input[contains(@class,'text-input text-input-ios')])[12]";
	public static final String estimatedLossField = "(//input[contains(@class,'text-input text-input-ios')])[13]";
	public static final String chooseDecisionSelect = "//select[contains(@placeholder,'Select')]";
	public static final String ckEditorField = "//div[@role='textbox']";
	
	public static final String searchBarDiagnoseField2 = "(//input[@class='searchbar-input'])[2]";
	public static final String clickSearchResultDiag2 = "/html/body/ion-app/ng-component/ion-nav/page-pmn-decision-approve-log/ion-content/div[2]/div/div/div/ion-grid/div[2]/div/ion-row[5]/ion-col[2]/ion-list/ion-item[1]/div[1]";
	public static final String diagnosisRemarksField2 = "(//input[contains(@class,'text-input text-input-ios')])[29]";
	public static final String declineAmountField = "(//input[contains(@class,'text-input text-input-ios')])[30]";
	public static final String actualLossField = "(//input[contains(@class,'text-input text-input-ios')])[31]";
	public static final String declineRemarksField = "(//input[contains(@class,'text-input text-input-ios')])[33]";
	public static final String declineReasonCodeField = "(//input[contains(@class,'text-input text-input-ios')])[34]";
	public static final String rejectDateField = "//input[@type='date']";
	
	public static final String clickSearchResultDiag3 = "/html/body/ion-app/ng-component/ion-nav/page-pmn-decision-approve-gop/ion-content/div[2]/div/div/ion-grid/div/ion-row[4]/ion-col[2]/ion-list/ion-item[1]/div[1]/div";
	public static final String diagnosisRemarksField3 = "(//input[contains(@class,'text-input text-input-ios')])[28]";
	public static final String declineAmountField3 = "(//input[contains(@class,'text-input text-input-ios')])[29]";
	public static final String actualLossField3 = "(//input[contains(@class,'text-input text-input-ios')])[30]";
	public static final String declineRemarksField3 = "(//input[contains(@class,'text-input text-input-ios')])[32]";
	public static final String declineReasonCodeField3 = "(//input[contains(@class,'text-input text-input-ios')])[33]";
	public static final String rejectDateField3 = "//input[@type='date']";
	
	public static final String saveDeclineReasonButton = "//button[contains(.,'Save')]";
	public static final String addQuestionButton = "//span[@class='button-inner'][contains(.,'Add Question')]";
	public static final String questionTextBox = "//textarea[@placeholder='Enter Question']";
	public static final String hourSuspendField = "(//input[@type='text'])[17]";
	public static final String tdkDijaminField = "(//input[@type='text'])[71]";
	public static final String submitButton = "//button[contains(.,'Submit')]";
	public static final String actCaseAccordion = "//strong[contains(.,'Active Case')]";
	public static final String docAccordion = "//strong[contains(.,'Document')]";
	public static final String decisionCommAccordion = "//strong[contains(.,'Decision And Comment')]";
	
	// Caselog Tab
	public static final String caselogUrl = "https://pruhub-sit.pru.intranet.asia/pruhub-2-pmn/#/pmn-caselog";
	public static final String caselogTab = "//a[contains(.,'Case Log')]";
	public static final String logByUsername = "(//span[@class='button-inner'])[6]";
	public static final String logByUsernameAndSystem = "(//span[@class='button-inner'])[7]";
	
	// Document Cm
	public static final String documentCmUrl = "https://pruhub-sit.pru.intranet.asia/pruhub-2-pmn/#/pmn-document-cm";
	public static final String documentCmTab = "//a[contains(.,'Document CM')]";
	
	// LOG Tab
	public static final String logUrl = "https://pruhub-sit.pru.intranet.asia/pruhub-2-pmn/#/pmn-decision-approve-log";
	public static final String logTab = "//a[contains(.,'LOG')]";
	public static final String logBackToDecisionTab = "//ion-label[contains(.,'Back to Decision & Comment')]";
	
	// GOP Tab
	public static final String gopUrl = "https://pruhub-sit.pru.intranet.asia/pruhub-2-pmn/#/pmn-decision-approve-gop";
	public static final String gopTab = "//a[contains(.,'GOP')]";
	public static final String gopBackToDecisionTab = logBackToDecisionTab;
	
	// Hospitalization Tab
	public static final String hospitalizationUrl = "https://pruhub-sit.pru.intranet.asia/pruhub-2-pmn/#/pmn-hospitalisation";
	public static final String hospitalizationTab = "//a[contains(.,'Hospitalization')]";
	public static final String showHideInformation = "//div[@class='header two-btn margin-right-0']";
	
//	Manage Task
	public static final String pmnManageTaskUrl = "https://pruhub-sit.pru.intranet.asia/pruhub-2-pmn/#/pmn-manage-task";
	public static final String pmnManageTask = "//div[@class='input-wrapper'][contains(.,'PMN Manage Task')]";
	public static final String policyNoField = "//input[@placeholder='Type Prior Approval Number here']";
	public static final String searchButton = "//span[contains(.,'Start Searching')]";
	public static final String searchResult = "/html/body/ion-app/ng-component/ion-nav/page-pmn-manage-task/ion-content/div[2]/div[4]/div[2]/div/div/ion-grid/ion-row[3]/ion-col/h6";
	public static final String transferButton = "(//span[@class='button-inner'])[7]";
	public static final String taskSelectButton = "//*[@id=\"undefined\"]";
	public static final String takeActionButton = "//span[contains(.,'Take Action Case')]";
	public static final String searchUser = "//input[@class='searchbar-input']";
	public static final String clickUser = "//ion-label[contains(.,'indraar')]";
	public static final String releaseButton = "(//span[@class='button-inner'])[9]";
	
	
//	public static final String a = "";
//	public static final String a = "";
//	public static final String a = "";
//	public static final String a = "";
//	public static final String a = "";
//	public static final String a = "";
//	public static final String a = "";
//	public static final String a = "";
//	public static final String a = "";
//	public static final String a = "";
//	public static final String a = "";
//	public static final String a = "";
//	public static final String a = "";
//	public static final String a = "";
//	public static final String a = "";
//	public static final String a = "";
//	public static final String a = "";
//	public static final String a = "";
//	public static final String a = "";
//	public static final String a = "";
//	public static final String a = "";
//	public static final String a = "";
//	public static final String a = "";
//	public static final String a = "";
	
	
	
		
}
