package PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import configs.ElementsInspect;
import utility.ClassHelp;
import utility.Hooks;

public class MenuPage extends Hooks {

	public MenuPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	WebDriverWait wait = new WebDriverWait(driver,60);
	
	By OpenMenu				= By.xpath(ElementsInspect.openMenu);
	By PmnWorkbasket		= By.xpath(ElementsInspect.pmnWorkbasket);
	By pmnManageTask		= By.xpath(ElementsInspect.pmnManageTask);
	
	String workbasketUrl	= ElementsInspect.pmnWrokbasketUrl;
	String manageTaskUrl	= ElementsInspect.pmnManageTaskUrl;
	
	public void getMenu() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(OpenMenu));
		driver.findElement(OpenMenu).click();
	}
	
	public void getWorkbasket() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(PmnWorkbasket));
		driver.findElement(PmnWorkbasket).click();
		ClassHelp.waitUntilUrlOpened(driver, workbasketUrl);
		
	}
	
	public void getManageTask() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(pmnManageTask));
		driver.findElement(pmnManageTask).click();
		ClassHelp.waitUntilUrlOpened(driver, manageTaskUrl);
	}
}
