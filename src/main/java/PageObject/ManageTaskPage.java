package PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import configs.ElementsInspect;
import utility.Hooks;

public class ManageTaskPage extends Hooks {
	
	public ManageTaskPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	WebDriverWait wait = new WebDriverWait(driver,60);
	DecisionPage logAssesser = new DecisionPage(driver);
	
	
	By toastMsg				= By.xpath(ElementsInspect.toastMsg);
	By policyNoField		= By.xpath(ElementsInspect.policyNoField);
	By searchButton			= By.xpath(ElementsInspect.searchButton);
	By taskTitle			= By.xpath(ElementsInspect.searchResult);
	By transferButton		= By.xpath(ElementsInspect.transferButton);
	By taskSelectButton		= By.xpath(ElementsInspect.taskSelectButton);
	By takeActionButton		= By.xpath(ElementsInspect.takeActionButton);
	By searchUser			= By.xpath(ElementsInspect.searchUser);
	By clickUser			= By.xpath(ElementsInspect.clickUser);
	By releaseButton		= By.xpath(ElementsInspect.releaseButton);
	
	
	
	public void searchTask(String policy) throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(searchButton));
		driver.findElement(policyNoField).clear();
		driver.findElement(policyNoField).sendKeys(policy);
		driver.findElement(searchButton).click();
	}
	
	public void getTaskTitle() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(taskSelectButton));
		String text = driver.findElement(taskTitle).getText();
		System.out.println(text);
	}
	
	public void transferTask(String user) throws InterruptedException {
		driver.findElement(transferButton).click();
		driver.findElement(taskSelectButton).click();
		driver.findElement(searchUser).sendKeys(user);
		wait.until(ExpectedConditions.elementToBeClickable(clickUser));
		driver.findElement(clickUser).click();
		driver.findElement(takeActionButton).click();
		wait.until(ExpectedConditions.elementToBeClickable(toastMsg));
		driver.findElement(toastMsg).click();
	}
	
	public void releaseTask() throws InterruptedException {
		driver.findElement(releaseButton).click();
		driver.findElement(taskSelectButton).click();
		driver.findElement(takeActionButton).click();
		wait.until(ExpectedConditions.elementToBeClickable(toastMsg));
		driver.findElement(toastMsg).click();
		Thread.sleep(3000);
	}
	
	public void transferTask2(String user) throws InterruptedException {
		driver.findElement(transferButton).click();
		driver.findElement(searchUser).sendKeys(user);
		wait.until(ExpectedConditions.elementToBeClickable(clickUser));
		driver.findElement(clickUser).click();
		driver.findElement(takeActionButton).click();
		wait.until(ExpectedConditions.elementToBeClickable(toastMsg));
		driver.findElement(toastMsg).click();
	}
}
