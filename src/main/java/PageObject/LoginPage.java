package PageObject;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import configs.ElementsInspect;
import utility.ClassHelp;
import utility.Hooks;
import utility.TestData;

public class LoginPage extends Hooks {
	
	By Username			= By.xpath(ElementsInspect.username);
	By Password			= By.xpath(ElementsInspect.password);
	By LoginClick		= By.xpath(ElementsInspect.loginButton);
	By LogoutClick		= By.xpath(ElementsInspect.logoutButton);
	By AcceptAlert		= By.xpath(ElementsInspect.acceptAlert);
	By MsgAlert			= By.xpath(ElementsInspect.msgAlert);
	
	String loginPageUrl	= ElementsInspect.loginPageUrl;
	String homepageUrl	= ElementsInspect.homepageUrl;
	
	private static TestData testData = new TestData();
	
	public void Homepage(WebDriver driver) {
		Hooks.driver = driver;
		PageFactory.initElements(driver, this);
	}	
	
	public void goToWebsite() {
		PortalPage(testData.getUrl());
	}
	
	public void PortalPage(String url) {
		driver.manage().window().maximize();
		driver.get(url.replace("\"", ""));
		ClassHelp.waitUntilUrlOpened(driver, loginPageUrl);			
	}
	
	public void userLogin() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		driver.findElement(Username).sendKeys(testData.getUsername());
		driver.findElement(Password).sendKeys(testData.getPassword());
		driver.findElement(LoginClick).click();		
		ClassHelp.waitUntilUrlOpened(driver, homepageUrl);
	}
	
	public void userLogout() throws InterruptedException {		
		WebElement logout = driver.findElement(LogoutClick);		
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", logout);		
		ClassHelp.waitUntilUrlOpened(driver, loginPageUrl);
	}	
	
//	Negative test	
	public void inputUsername(String arg1) {
		driver.findElement(Username).sendKeys(arg1);
		System.out.println("Negative Username : " + arg1);
		Assert.assertEquals(arg1, arg1);
	}
	
	public void inputPassword(String arg1) {
		driver.findElement(Password).sendKeys(arg1);
		System.out.println("Negative Password : " + arg1);
		Assert.assertEquals(arg1, arg1);
		System.out.println("Assert passed");
	}
	
	public void clickLogin() throws InterruptedException {
		driver.findElement(LoginClick).click();
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);	
	}
	
	public void okAlert() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		driver.findElement(AcceptAlert).click();
	}	
}
