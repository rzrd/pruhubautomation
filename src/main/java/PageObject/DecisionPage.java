package PageObject;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import configs.ElementsInspect;
import utility.ClassHelp;
import utility.Hooks;

public class DecisionPage extends Hooks {
	
	public DecisionPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	WebDriverWait wait = new WebDriverWait(driver,60);	
	
	By TitleTask			= By.xpath(ElementsInspect.titleTask);
	By DecisionCommentTab	= By.xpath(ElementsInspect.decisionCommentTab);
	By SelectDecision		= By.xpath(ElementsInspect.chooseDecisionSelect);
	By diagnoseField		= By.xpath(ElementsInspect.searchBarDiagnoseField);
	By clickResult 			= By.xpath(ElementsInspect.clickSearchResultDiag);
	By diagRemarkField		= By.xpath(ElementsInspect.diagnosisRemarksField);
	By estAmountField		= By.xpath(ElementsInspect.estimatedAmountField);
	By estLosField			= By.xpath(ElementsInspect.estimatedLossField);
	By diagnoseField2		= By.xpath(ElementsInspect.searchBarDiagnoseField2);
	By clickResult2 		= By.xpath(ElementsInspect.clickSearchResultDiag2);
	By diagRemarkField2		= By.xpath(ElementsInspect.diagnosisRemarksField2);
	By decAmountField		= By.xpath(ElementsInspect.declineAmountField);
	By actLosField			= By.xpath(ElementsInspect.actualLossField);
	By decRemarksField		= By.xpath(ElementsInspect.declineRemarksField);
	By decReasonField		= By.xpath(ElementsInspect.declineReasonCodeField);
	By rejectDate			= By.xpath(ElementsInspect.rejectDateField);
	By saveDeclineButton	= By.xpath(ElementsInspect.saveDeclineReasonButton);
	By addQuestion			= By.xpath(ElementsInspect.addQuestionButton);
	By questionTextBox		= By.xpath(ElementsInspect.questionTextBox);
	By ckTextEditor			= By.xpath(ElementsInspect.ckEditorField);
	By submitButton			= By.xpath(ElementsInspect.submitButton);
	By hourSuspendField	= By.xpath(ElementsInspect.hourSuspendField);
	By tdkJaminField		= By.xpath(ElementsInspect.tdkDijaminField);
	By clickResult3	 		= By.xpath(ElementsInspect.clickSearchResultDiag3);
	By diagRemarkField3		= By.xpath(ElementsInspect.diagnosisRemarksField3);
	By decAmountField3		= By.xpath(ElementsInspect.declineAmountField3);
	By actLosField3			= By.xpath(ElementsInspect.actualLossField3);
	By decRemarksField3		= By.xpath(ElementsInspect.declineRemarksField3);
	By decReasonField3		= By.xpath(ElementsInspect.declineReasonCodeField3);
	
	String workbasketUrl	= ElementsInspect.pmnWrokbasketUrl;
	String decisionUrl	= ElementsInspect.decisionCommentUrl;
	
	
	public void getDecisionTab() throws InterruptedException {		
		wait.until(ExpectedConditions.elementToBeClickable(DecisionCommentTab));	
		try {
			driver.findElement(DecisionCommentTab).click();
		}
		catch(StaleElementReferenceException ex) {
			driver.findElement(DecisionCommentTab).click();
		}		
	    ClassHelp.waitUntilUrlOpened(driver, decisionUrl);
	}
	
	public void getSelectDecision(String select) throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
		Select chooseDecision = new Select(driver.findElement(SelectDecision));
		chooseDecision.selectByVisibleText(select);		
		System.out.println(select + " has been choosen");
	}
	
	public void writeNote(String note) throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
		driver.findElement(ckTextEditor).sendKeys(note);
//		Thread.sleep(3000);
	}
	
	public void inputDiagnose(String diagnose, String remark) throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		driver.findElement(diagnoseField).sendKeys(diagnose);
		
		wait.until(ExpectedConditions.elementToBeClickable(clickResult));
		driver.findElement(clickResult).click();
		driver.findElement(diagRemarkField).sendKeys(remark);
	}
	
	public void inputEstimate(String amount, String los) throws InterruptedException {
		driver.findElement(estAmountField).sendKeys(amount);
		driver.findElement(estLosField).sendKeys(los);
	}
	
	public void submitDecision() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(submitButton));
		try {
			driver.findElement(submitButton).click();
			System.out.println("submit clicked");
			ClassHelp.waitUntilUrlOpened(driver, workbasketUrl);
		  } catch (Exception e) {
		     JavascriptExecutor executor = (JavascriptExecutor) driver;
		     executor.executeScript("arguments[0].click();", driver.findElement(submitButton));
		     System.out.println("submit clickedd");
		     ClassHelp.waitUntilUrlOpened(driver, workbasketUrl);
		  }
	}
	
	public void inputDiagnoseDecline(String diagnose, String remark) throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		driver.findElement(diagnoseField2).sendKeys(diagnose);
		wait.until(ExpectedConditions.elementToBeClickable(clickResult2));
		driver.findElement(clickResult2).click();
		driver.findElement(diagRemarkField2).sendKeys(remark);
	}
	
	public void inputAmountLosDecline(String amount, String los) throws InterruptedException {
		driver.findElement(decAmountField).sendKeys(amount);
		driver.findElement(actLosField).sendKeys(los);
	}
	
	public void inputRejectDate(String date) throws InterruptedException {
		driver.findElement(rejectDate).sendKeys(date);
//		coba set biar date otomatis di hari yg sama
	}
	
	public void inputReasoneDecline(String remarks, String reason) throws InterruptedException {
		driver.findElement(decRemarksField).sendKeys(remarks);
		driver.findElement(decReasonField).sendKeys(reason);
	}
	
	public void saveDecline() throws InterruptedException {
		driver.findElement(saveDeclineButton).click();
	}
	
	public void clickAddQuestion() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(addQuestion));
		driver.findElement(addQuestion).click();
	}
	
	public void writeQuestion(String question) throws InterruptedException {
		driver.findElement(questionTextBox).sendKeys(question);
	}
	
	public void inputHourSuspend(String hour) throws InterruptedException {
		driver.findElement(hourSuspendField).sendKeys(hour);
	}
	
	public void inputTidakDijaminField(String text) throws InterruptedException {
		wait.until(ExpectedConditions.invisibilityOfElementLocated(tdkJaminField));
		driver.findElement(tdkJaminField).sendKeys(text);
	}
	
	public void inputDiagnoseDecline3(String diagnose, String remark) throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		driver.findElement(diagnoseField2).sendKeys(diagnose);
		wait.until(ExpectedConditions.elementToBeClickable(clickResult3));
		driver.findElement(clickResult3).click();
		driver.findElement(diagRemarkField3).sendKeys(remark);
	}
	
	public void inputAmountLosDecline3(String amount, String los) throws InterruptedException {
		driver.findElement(decAmountField3).sendKeys(amount);
		driver.findElement(actLosField3).sendKeys(los);
	}
	
	public void inputReasoneDecline3(String remarks, String reason) throws InterruptedException {
		driver.findElement(decRemarksField3).sendKeys(remarks);
		driver.findElement(decReasonField3).sendKeys(reason);
	}

	
}
