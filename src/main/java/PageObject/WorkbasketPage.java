package PageObject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import configs.ElementsInspect;
import utility.Hooks;

public class WorkbasketPage  extends Hooks {

	public WorkbasketPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	WebDriverWait wait = new WebDriverWait(driver,60);
	
	By policyNumber		= By.xpath(ElementsInspect.policyNumber);
	By selectTask		= By.xpath(ElementsInspect.selectTask);
	By titleTask		= By.xpath(ElementsInspect.titleTask);
	
	public void getCase(String find) throws InterruptedException {		
		policyNumber = By.xpath("//span[@class='last-decision'][contains(.,'Admisson ID : "+find+"')]");
		wait.until(ExpectedConditions.elementToBeClickable(policyNumber));
		driver.findElement(policyNumber).click();
	}
	
	public void getfirstcase(String find) throws InterruptedException {
//		"find" argument is activity name ex: PMN_LOG_ASSESSOR - PMN LOG ASSESSOR
		wait.until(ExpectedConditions.elementToBeClickable(titleTask));
		List<WebElement> items = driver.findElements(titleTask);
		int l = items.size();
		if (l == 0) {
			System.out.println("there is no task with " + find +" activity today");
			driver.close();
		} else {			
			for(int i=0; i<l; i++) {
				String text = items.get(i).getText();
				if(text.contentEquals(find)) {
					int num = i + 1;
					System.out.println(i+ text+ find +num);
					selectTask = By.xpath("(//ion-col[contains(@class,'task-name padding-top-20 col')])["+num+"]");
					driver.findElement(selectTask).click();
					System.out.println("open detail task done");
					break;
				}
			} 
		}
	}
	
	
	
}