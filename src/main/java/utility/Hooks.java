package utility;

import java.io.File;
import java.net.MalformedURLException;
import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class Hooks {
	
	static String firefoxDriverPath = System.getProperty("user.dir")+File.separator+"src"+File.separator+"driver"+File.separator+ClassHelp.getEnv("mozillaFirefox");
	static String chromeDriverPath = System.getProperty("user.dir")+File.separator+"src"+File.separator+"driver"+File.separator+ClassHelp.getEnv("googleChrome");
	
	public static WebDriver driver;
	TestData testData = new TestData();
	
	public WebDriver openBrowser(String browser) throws MalformedURLException {
		if ("chrome".equals(browser)) {
			System.setProperty("webdriver.chrome.driver", chromeDriverPath);
			
			String downloadFilePath = System.getProperty("user.dir")+File.separator+"src"+File.separator+"test"+File.separator+"resources"+File.separator+"Download"+File.separator;
			HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
			chromePrefs.put("profile.default_content_setting.popups", 0);
			chromePrefs.put("download.default_directory", downloadFilePath);
			
			ChromeOptions chromeOptions = new ChromeOptions();
			chromeOptions.setExperimentalOption("prefs", chromePrefs);			
			if (testData.getIsHeadless().equalsIgnoreCase("yes")) {
				chromeOptions.addArguments("--headless");
			}
			
			chromeOptions.addArguments("--windows-size=1366, 768");
			driver = new ChromeDriver(chromeOptions);
		} else if ("firefox".equals(browser)){
			System.setProperty("webdriver.gecko.driver", firefoxDriverPath);
			
			FirefoxOptions firefoxOptions = new FirefoxOptions();
			if (testData.getIsHeadless().toLowerCase().equals("yes")) {
				firefoxOptions.addArguments("--headless");
			}
			firefoxOptions.addArguments("--windows-size=1366, 768");
			driver = new FirefoxDriver(firefoxOptions);			
		} 
//		else if () {
//			for another drive
//		}		
		else {
			System.out.println("Selected browser not supported - " + browser);
		}
		System.out.println("Opening Browser: " + browser);		
		return driver;
	}
	
//	public void takeScreenshot(Scenario scenario) {
//		
//	}
	
}
