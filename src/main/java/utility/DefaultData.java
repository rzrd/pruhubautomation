package utility;


public class DefaultData {
	
	public static TestData testData = new TestData();
	
	public static void setTestData(){
		testData.setUrl(ClassHelp.getEnv("url"));
		testData.setUserName(ClassHelp.getEnv("username"));
		testData.setPassword(ClassHelp.getEnv("password"));
	}
}
