package StepsDefinition;

import org.openqa.selenium.WebDriver;

import PageObject.DecisionPage;
import PageObject.MenuPage;
import PageObject.WorkbasketPage;
import configs.ElementsInspect;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utility.*;

public class LogAssesserDef {
	WebDriver driver;
	DecisionPage decisionPage = new DecisionPage(driver);
	MenuPage menuPage = new MenuPage(driver);
	WorkbasketPage workbasketPage = new WorkbasketPage(driver);
	
	String caseUrl	= ElementsInspect.caselogUrl;
	String rejectLogUrl	= ElementsInspect.rejectLogUrl;
	String decisionUrl	= ElementsInspect.decisionCommentUrl;
	String releaseLmlUrl	= ElementsInspect.releaseLmlUrl;
		
	@Given("^click menu$")
	public void click_menu() throws Throwable {
		menuPage.getMenu();
	}
	
	@Given("^go to workbasket menu$")
	public void go_to_workbasket_menu() throws Throwable {
		menuPage.getWorkbasket();		
	}

	@When("^select Log Assessor task with \"([^\"]*)\" Policy No which want to be decided$")
	public void select_Log_Assessor_task(String arg1) throws Throwable {
		workbasketPage.getCase(arg1);	
	}

	@When("^screen will be displayed with case detail on caselog tab$")
	public void screen_will_be_displayed_with_case_detail() throws Throwable {		
		ClassHelp.waitUntilUrlOpened(driver, caseUrl);		
	}

	@Then("^go to decision and comment tab$")
	public void go_to_decision_and_comment_tab() throws Throwable {
	    decisionPage.getDecisionTab();
	}

	@Then("^choose \"([^\"]*)\" decision$")
	public void choose_decision(String arg1) throws Throwable {
	   decisionPage.getSelectDecision(arg1);
	}

	@Then("^write note : \"([^\"]*)\"$")
	public void write_some_note_on_field(String arg1) throws Throwable {
	    decisionPage.writeNote(arg1);
	}
	
/********************
 * Reject LOG Scenario 
 */

	@Then("^search and select \"([^\"]*)\" diagnose and input \"([^\"]*)\" diagnosis remarks field$")
	public void input_diagnose(String arg1, String arg2) throws Throwable {
	    decisionPage.inputDiagnose(arg1,arg2);
	}

	@Then("^input \"([^\"]*)\" estimated amount and \"([^\"]*)\" estimated LOS field$")
	public void input_estimate(String arg1, String arg2) throws Throwable {
	    decisionPage.inputEstimate(arg1, arg2);
	}

	@Then("^click Submit to submit decision$")
	public void click_Submit_to_submit_decision() throws Throwable {
		decisionPage.submitDecision();
	}

	@Then("^screen reject log will be displayed$")
	public void screen_reject_log_will_be_displayed() throws Throwable {
		ClassHelp.waitUntilUrlOpened(driver, rejectLogUrl);
	}
	
	@Then("^search and select \"([^\"]*)\" diagnose and input \"([^\"]*)\" diagnosis remarks Reject LOG field$")
	public void input_diagnose_reject_log(String arg1, String arg2) throws Throwable {
	    decisionPage.inputDiagnoseDecline(arg1, arg2);
	}

	@Then("^input \"([^\"]*)\" amount decline and \"([^\"]*)\" actual LOS Reject LOG field$")
	public void input_estimate_reject_log(String arg1, String arg2) throws Throwable {
	    decisionPage.inputAmountLosDecline(arg1, arg2);
	}

	@Then("^input discharge date$")
	public void input_discharge_date() throws Throwable {
		decisionPage.inputRejectDate("1604");
	}

	@Then("^input \"([^\"]*)\" decline remarks and \"([^\"]*)\" reason decline code$")
	public void input_decline_remark(String arg1, String arg2) throws Throwable {
	    decisionPage.inputReasoneDecline(arg1, arg2);
	}

	@Then("^click save$")
	public void click_save() throws Throwable {
	    decisionPage.saveDecline();
	}

	@Then("^decision & comment tab will be displayed$")
	public void decision_comment_tab_will_be_displayed() throws Throwable {
		ClassHelp.waitUntilUrlOpened(driver, decisionUrl);
	}
	
/********************
 * Release LML Scenario
 */

	@Then("^screen Release LML will be displayed$")
	public void screen_Release_LML_will_be_displayed() throws Throwable {
		ClassHelp.waitUntilUrlOpened(driver, releaseLmlUrl);
	}

	@Then("^click add question$")
	public void click_add_question() throws Throwable {
	    decisionPage.clickAddQuestion();
	}

	@Then("^enter \"([^\"]*)\" in question box$")
	public void enter_in_question_box(String arg1) throws Throwable {
	    decisionPage.writeQuestion(arg1);
	}
}
