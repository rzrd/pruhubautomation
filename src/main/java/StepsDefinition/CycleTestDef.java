package StepsDefinition;

import org.openqa.selenium.WebDriver;

import PageObject.ManageTaskPage;
import PageObject.MenuPage;
import PageObject.DecisionPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CycleTestDef {

	WebDriver driver;
	DecisionPage logAssesser = new DecisionPage(driver);
	ManageTaskPage manageTask = new ManageTaskPage(driver);
	MenuPage menu = new MenuPage(driver);
	
	@Given("^go to manage task menu$")
	public void go_to_manage_task_menu() throws Throwable {
		menu.getManageTask();
	}

	@Given("^find task based on their policy number \"([^\"]*)\"$")
	public void find_task_based_on_their_policy_number(String arg1) throws Throwable {
	    manageTask.searchTask(arg1);
	}

	@When("^task search result has shown$")
	public void task_search_result_has_shown() throws Throwable {
	    manageTask.getTaskTitle();
	}

	@Then("^transfer task to user \"([^\"]*)\"$")
	public void transfer_task_to_user(String arg1) throws Throwable {
	    manageTask.transferTask(arg1);
	}

	@Then("^click Submit to submit log assessor decision$")
	public void click_Submit_to_submit_log_assessor_decision() throws Throwable {
		logAssesser.submitDecision();
	}

	@Then("^release suspend task$")
	public void release_suspend_task() throws Throwable {
	    manageTask.releaseTask();
	}
	@Then("^do transfer task to user \"([^\"]*)\"$")
	public void do_transfer_task_to_user(String arg1) throws Throwable {
	    manageTask.transferTask2(arg1);
	}

	@Then("^click Submit to submit case monitoring decision$")
	public void click_Submit_to_submit_case_monitoring_decision() throws Throwable {
		logAssesser.submitDecision();
	}

	@Then("^test done$")
	public void test_done() throws Throwable {
	    driver.close();
	}
}




