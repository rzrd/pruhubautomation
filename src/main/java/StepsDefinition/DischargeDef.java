package StepsDefinition;

import org.openqa.selenium.WebDriver;

import PageObject.DecisionPage;
import PageObject.WorkbasketPage;
import PageObject.MenuPage;
import configs.ElementsInspect;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import utility.ClassHelp;

public class DischargeDef {
	WebDriver driver;
	DecisionPage decisionPage = new DecisionPage(driver);
	MenuPage menuPage = new MenuPage(driver);
	WorkbasketPage workbasketPage = new WorkbasketPage(driver);
	
	String approveGopUrl	= ElementsInspect.approveGopUrl;
	String rejectGopUrl	= ElementsInspect.rejectGopUrl;

	
	@Given("^select Discharge task which \"([^\"]*)\" Policy No which want to be decided$")
	public void select_Discharge_task(String arg1) throws Throwable {
		workbasketPage.getCase(arg1);
	}

	@Given("^screen Approve GOP will be displayed$")
	public void screen_Approve_GOP_will_be_displayed() throws Throwable {
	    ClassHelp.waitUntilUrlOpened(driver, approveGopUrl);
	}
	
	@Then("^search and select \"([^\"]*)\" diagnose and input \"([^\"]*)\" diagnosis remarks Reject GOP field$")
	public void input_diagnose_reject_gop(String arg1, String arg2) throws Throwable {
	    decisionPage.inputDiagnoseDecline3(arg1, arg2);
	}

	@Then("^input \"([^\"]*)\" amount decline and \"([^\"]*)\" actual LOS Reject GOP field$")
	public void input_estimate_reject_gop(String arg1, String arg2) throws Throwable {
		decisionPage.inputAmountLosDecline3(arg1, arg2);
	}
	
	@Then("^input \"([^\"]*)\" decline remarks and \"([^\"]*)\" reason decline code Reject GOP$")
	public void input_decline_remark_reject_gop(String arg1, String arg2) throws Throwable {
		decisionPage.inputReasoneDecline3(arg1, arg2);
	}

	@Given("^input \"([^\"]*)\" Tidak Dijamin field$")
	public void input_in_last_field(String arg1) throws Throwable {
		decisionPage.inputTidakDijaminField(arg1);
	}

	@Given("^screen reject GOP will be displayed$")
	public void screen_reject_GOP_will_be_displayed() throws Throwable {
	    ClassHelp.waitUntilUrlOpened(driver, rejectGopUrl);
	}
}
