package StepsDefinition;

import org.openqa.selenium.WebDriver;

import PageObject.DecisionPage;
import PageObject.WorkbasketPage;
import PageObject.MenuPage;
import configs.ElementsInspect;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utility.ClassHelp;

public class CaseMonDef {
	
	WebDriver driver;
	DecisionPage decisionPage = new DecisionPage(driver);
	MenuPage menuPage = new MenuPage(driver);
	WorkbasketPage workbasketPage = new WorkbasketPage(driver);
	
	String hospUrl	= ElementsInspect.hospitalizationUrl;
	String declineLogUrl	= ElementsInspect.declineLogUrl;
	
	@Given("^select Case Monitoring task with \"([^\"]*)\" Policy No which want to be decided$")
	public void select_Case_Monitoring_task(String arg1) throws Throwable {
		workbasketPage.getCase(arg1);
	}
	
	@When("^screen will be displayed with case detail at hospitalization tab$")
	public void screen_will_be_displayed_with_case_detail_hosp_tab() throws Throwable {
		ClassHelp.waitUntilUrlOpened(driver, hospUrl);
	}

	@Given("^screen decline log will be displayed$")
	public void screen_decline_log_will_be_displayed() throws Throwable {
	    ClassHelp.waitUntilUrlOpened(driver, declineLogUrl);
	}

	@Then("^search and select \"([^\"]*)\" diagnose and input \"([^\"]*)\" diagnosis remarks Decline LOG field$")
	public void input_diagnose_reject_log(String arg1, String arg2) throws Throwable {
	    decisionPage.inputDiagnoseDecline(arg1, arg2);
	}

	@Then("^input \"([^\"]*)\" amount decline and \"([^\"]*)\" actual LOS Decline LOG field$")
	public void input_estimate_reject_log(String arg1, String arg2) throws Throwable {
	    decisionPage.inputAmountLosDecline(arg1, arg2);
	}

	@Given("^input how much \"([^\"]*)\" user need to suspend case$")
	public void input_how_much_user_need_to_suspend_case(String arg1) throws Throwable {
		decisionPage.inputHourSuspend(arg1);
	}
}
