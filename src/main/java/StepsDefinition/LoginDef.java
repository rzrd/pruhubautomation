package StepsDefinition;

import org.openqa.selenium.WebDriver;
import PageObject.LoginPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utility.*;

public class LoginDef {

	WebDriver driver;
	Hooks hooks = new Hooks();
	TestData testData = new TestData();
	LoginPage loginPage = new LoginPage();
	
	@Given("^initiate browser$")
	public void initiate_browser() throws Throwable {
//	   testData.setIsHeadless(ClassHelp.getEnv("DontRunningBrowser"));
	   testData.setCloseBrowserEveryScenario(ClassHelp.getEnv("CloseBrowser"));
//	   hooks = new Hooks();
	   System.out.println("Hooks set : " + hooks);
	   driver = hooks.openBrowser(ClassHelp.getEnv("browser"));
	   System.out.println("Driver set : " + driver);
	}
	
	@Given("^setup data for test$")
	public void setup_data_for_test() throws Throwable {
	    DefaultData.setTestData();
	    System.out.println("URL set : " +testData);
	    ClassHelp.getEnv("url");
	}

	@Given("^navigates to Pruhub login page$")
	public void navigates_Pruhub_login_page() throws Throwable {
	    loginPage.goToWebsite();
	}	

	@Given("^user login to Pruhub$")
	public void user_login_to_Pruhub() throws Throwable {
	    DefaultData.setTestData();
	    System.out.println("User data set :" + testData);
	    loginPage.userLogin();
	}

	@When("^go to Pruhub landing page$")
	public void go_to_Pruhub_landing_page() throws Throwable {
		driver.getTitle();
	}

	@Then("^user enable to logout$")
	public void user_enable_to_logout() throws Throwable {
	    loginPage.userLogout();
	}
	
	@When("^user input \"([^\"]*)\" to username textbox$")
	public void user_input_to_username_textbox(String arg1) throws Throwable {
	    loginPage.inputUsername(arg1);
	}

	@When("^user input \"([^\"]*)\" to password textbox$")
	public void user_input_to_password_textbox(String arg1) throws Throwable {
	   loginPage.inputPassword(arg1);
	}

	@Then("^click login button$")
	public void click_login_button() throws Throwable {
	    loginPage.clickLogin();
	}
	
	@Then("^click OK when Login Failed alert has shown$")
	public void click_OK_when_Login_Failed_alert_has_shown() throws Throwable {
		loginPage.okAlert();
	}
}
