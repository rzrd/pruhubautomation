package TestRunner;
import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import org.testng.annotations.AfterClass;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/Scenario/",
        glue = {"StepsDefinition"},
        tags = {"@TestCycle"},
        plugin = {"com.cucumber.listener.ExtentCucumberFormatter:target/htmlReport/PruhubAutoTestreport.html","json:target/jsonReport/PruhubAutoTestReport.json"},
        dryRun = false,
        monochrome = true
)
public class RunnerTest {
    @AfterClass
    public static void writeExtentReport() {
        Reporter.loadXMLConfig("src/resources/extent-config.xml");
        Reporter.setSystemInfo("Author ","Rozi");
        Reporter.setSystemInfo("Time Zone ", System.getProperty("user.timezone"));
        Reporter.setSystemInfo("Machine ", 	"Windows 10 " + " 64 Bit");
        Reporter.setSystemInfo("CUCUMBER SELENIUM : ", "PMN" + "Hospital Portal");
        Reporter.setSystemInfo("PMN ","Hospital-Portal");
    }
}

