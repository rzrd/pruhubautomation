@Login
Feature: Homepage
 	As a User,
	I want to see my dashboard, workbasket and inquiry transaction based on my role.
	So that I can work based on my pending task.

  Background: 
    Given initiate browser
    Given setup data for test
    Given navigates to Pruhub login page   

  Scenario: User able succesfully Login
    Given user login to Pruhub
    And go to Pruhub landing page
    Then user enable to logout
    
	Scenario Outline: Negative Login
	 	When user input "<Username>" to username textbox
	 	And user input "<Password>" to password textbox
	 	Then click login button
	 	And click OK when Login Failed alert has shown
	 	Examples:
	 		| Username			| Password			|
	 		| newUser			| WrongP4$$worD		|