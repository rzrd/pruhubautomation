@TestCycle
Feature: One Time Cycle Test
	As a user
	I want to see one case running from start to end activity
	So that I can make sure that cycle works fine

	Background:
		Given initiate browser
		Given setup data for test
		Given navigates to Pruhub login page
		
	Scenario Outline: Log Assessor
		Given user login to Pruhub 
		And click menu
		And go to manage task menu
		And find task based on their policy number "<Policy No>"
		When task search result has shown
		Then transfer task to user "<User>"	
		And go to workbasket menu
		And select Log Assessor task with "<Policy No>" Policy No which want to be decided
		And screen will be displayed with case detail on caselog tab
		And go to decision and comment tab
		And choose "Approve LOG" decision
		And write note : "<Note>"
		And search and select "fev" diagnose and input "tuborculosis" diagnosis remarks field
		And input "1000000" estimated amount and "3.5" estimated LOS field
		And click Submit to submit log assessor decision
		And find task based on their policy number "<Policy No>"
		And task search result has shown
		And release suspend task
		
		Examples:
		|Policy No		|User			|Note			|
		|SGKAB5			|indraar		|Approve LOG ->	|
		
	Scenario Outline: Case Monitoring And Discharge
		Given user login to Pruhub
		And click menu
		And go to manage task menu
		And find task based on their policy number "<Policy No>"
		And task search result has shown
		And release suspend task
		And do transfer task to user "<User>"
		And go to workbasket menu
		And select Case Monitoring task with "<Policy No>" Policy No which want to be decided
		And screen will be displayed with case detail at hospitalization tab
		And go to decision and comment tab
		And choose "Move to Discharge" decision
		And write note : "<Note1>"		
		And click Submit to submit case monitoring decision
		And go to manage task menu
		And find task based on their policy number "<Policy No>"
		And task search result has shown
		And release suspend task
		And do transfer task to user "<User>"
		And go to workbasket
		And select Discharge task which "<Policy No>" Policy No which want to be decided
		When screen will be displayed with case detail at hospitalization tab
		Then go to decision and comment tab
		And choose "Approve GOP" decision
		And screen Approve GOP will be displayed
		And input "<Tidak Dijamin>" Tidak Dijamin field
		And click save
		And decision & comment tab will be displayed
		And write note : "<Note2>"
		And click Submit to submit decision
		
		Examples:
		|Policy No		|User			|Note1					|Note2			|
		|8BAF7O			|indraar		|Move to discharge ->	|Approve GOP.	|
		
		