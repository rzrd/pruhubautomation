@LogAssesser
Feature: Log Asseser
	As a Log Assessor user
	I want to see my workbasket task detail
	So that I can write remarks before take decision per case

	Background:
		Given initiate browser
		Given setup data for test
		Given navigates to Pruhub login page	
		
	Scenario Outline: See Log Assessor task detail and choose decision Reject LOG
		Given user login to Pruhub
		And click menu	
		And go to workbasket menu
		And select Log Assessor task with "<Policy No>" Policy No which want to be decided
		When screen will be displayed with case detail on caselog tab
		Then go to decision and comment tab
		And choose "Reject LOG" decision
		And screen reject log will be displayed
		And search and select "fev" diagnose and input "tuberculosis" diagnosis remarks Reject LOG field
		And input "<Amount Decline>" amount decline and "<Actual LOS>" actual LOS Reject LOG field
		And input discharge date
		And input "<Decline Remarks>" decline remarks  and "<Reason Decline Code>" reason decline code
		And click save
		And decision & comment tab will be displayed
		And search and select "fev" diagnose and input "tuberculosis" diagnosis remarks field
		And input "1000000" estimated amount and "3.5" estimated LOS field
		And write note : "<Note>"
		And click Submit to submit decision
		
		Examples:
		|Policy No	|Amount Decline	|Actual LOS	|Decline Remarks	|Reason Decline Code	|Note			|
		|FOD2BU		|1000000		|2			|dokumen tdk lengkap|02						|Reject LOG.	|
	
	Scenario Outline: See Log Assessor task detail and choose decision Release LML
		Given user login to Pruhub
		And click menu
		And go to workbasket menu
		And select Log Assessor task with "<Policy No>" Policy No which want to be decided
		When screen will be displayed with case detail on caselog tab
		Then go to decision and comment tab
		And choose "Release LML" decision
		And screen Release LML will be displayed
		And click add question
		And enter "<Question>" in question box
		And click save
		And decision & comment tab will be displayed
		And search and select "fev" diagnose and input "tuberculosis" diagnosis remarks field
		And input "1000000" estimated amount and "3.5" estimated LOS field
		And write note : "<Note>"
		And click Submit to submit decision
		
		Examples:
		|Policy No	|Question	|Note		|
		|7HEWSI		|How much?	|Rls LML ->	|

	Scenario Outline: See Log Assessor task detail and choose decision
		Given user login to Pruhub
		And click menu
		And go to workbasket menu
		And select Log Assessor task with "<Policy No>" Policy No which want to be decided
		When screen will be displayed with case detail on caselog tab
		Then go to decision and comment tab
		And choose "<Choose Decision>" decision
		And write note : "<Note>"
		And search and select "fev" diagnose and input "tuborculosis" diagnosis remarks field
		And input "1000000" estimated amount and "3.5" estimated LOS field
		And click Submit to submit decision
		
		Examples:
		|Choose Decision			|Note						|Policy No	|
		|Approve LOG				|Approve LOG ->				|WMF75A		|
		|Approve LOG & Discharge	|Approve LOG & discharge ->	|Q6HAB0		|
		|Suspend					|Suspend ->					|4INI0F		|
#		|Hold						|choose Hold				|0			|
#		|Transfer to Internal Doctor|choose trf to Int. Doctor	|0			|
#		|Transfer to Investigator	|choose trf to inv			|0			|
#		|Back to user				|choose Back to user		|0			|

		