@CaseMonitoring
Feature: Case Monitoring
	As a Case Monitoring user,
	I want to see my workbasket task detail.
	So that I can write remarks before take decision per case

	Background:
		Given initiate browser
		Given setup data for test
		Given navigates to Pruhub login page
		
	Scenario Outline: See Case Monitoring task detail and choose decision
		Given user login to Pruhub
		And click menu	
		And go to workbasket menu
		And select Case Monitoring task with "<Policy No>" Policy No which want to be decided
		When screen will be displayed with case detail at hospitalization tab
		Then go to decision and comment tab
		And choose "<Choose Decision>" decision
		And write note : "<Note>"		
		And click Submit to submit decision
		
		Examples:
		|Choose Decision			|Note				|Policy No	|
		|Complete With Excess		|Comp w excess ->	|4INI0F 	|
		|Complete Without Excess	|comp wo excess ->	|7HEWSI		|
		|Move to Discharge			|move to disch ->	|WMF75A		|
		
	Scenario Outline: See Case Monitoring task detail and choose decision Decline LOG
		Given user login to Pruhub
		And click menu	
		And go to workbasket menu
		And select Case Monitoring task with "<Policy No>" Policy No which want to be decided
		When screen will be displayed with case detail at hospitalization tab
		Then go to decision and comment tab
		And choose "Decline LOG" decision
		And screen decline log will be displayed
		And search and select "fev" diagnose and input "tuberculosis" diagnosis remarks Decline LOG field
		And input "100000" amount decline and "3.5" actual LOS Decline LOG field
		And input discharge date
		And input "<Decline Remarks>" decline remarks and "<Reason Decline Code>" reason decline code
		And click save
		And decision & comment tab will be displayed
		And write note : "<Note>"
		And click Submit to submit decision
		
		Examples:
		|Policy No	|Decline Remarks	|Reason Decline Code	|Note			|
		|Q6HAB0		|tdk lengkap		|02						|decline LOG.	|
		
	Scenario Outline: See Case Monitoring task detail and choose decision Temporary Suspend
		Given user login to Pruhub
		And click menu	
		And go to workbasket menu
		And select Case Monitoring task with "<Policy No>" Policy No which want to be decided
		When screen will be displayed with case detail at hospitalization tab
		Then go to decision and comment tab
		And choose "Temporary Suspend" decision
		And input how much "<Hours>" user need to suspend case
		And write note : "<Note>"	
		And click Submit to submit decision
		
		Examples:
		|Policy No	|Hours	|Note				|
		|ON6J0H		|4		|Temp Suspend ->	|
		
	