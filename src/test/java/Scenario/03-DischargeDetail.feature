@Discharge
Feature: Discharge
	As a Discharge user,
	I want to see my workbasket task detail.
	So that I can write remarks before take decision per case

	Background:
		Given initiate browser
		Given setup data for test
		Given navigates to Pruhub login page
		
	Scenario Outline: See Discharge task detail and choose decision
		Given user login to Pruhub
		And click menu
		And go to workbasket menu
		And select Discharge task which "<Policy No>" Policy No which want to be decided
		When screen will be displayed with case detail at hospitalization tab
		Then go to decision and comment tab
		And choose "<Choose Decision>" decision
		And write note : "<Note>"
		And click Submit to submit decision
		
		Examples:
		|Choose Decision				|Note		|Policy No	|
		|Suspend						|Suspend ->	|FOD2BU		|
#		|Transfer to Internal Doctor	|Oke4		|			|
#		|Transfer to Investigator		|Oke5		|			|
#		|Hold							|oke6		|			|
		
	Scenario Outline: See Discharge task detail and choose decision Approve GOP
		Given user login to Pruhub
		And click menu	
		And go to workbasket menu
		And select Discharge task which "<Policy No>" Policy No which want to be decided
		When screen will be displayed with case detail at hospitalization tab
		Then go to decision and comment tab
		And choose "Approve GOP" decision
		And screen Approve GOP will be displayed
		And input "<Tidak Dijamin>" Tidak Dijamin field
		And click save
		And decision & comment tab will be displayed
		And write note : "<Note>"
		And click Submit to submit decision
		
		Examples:
		|Tidak Dijamin	|Note			|Policy No	|
		|200000			|Approev GOP.	|WMF75A 	|
		
	Scenario Outline: See Discharge task detail and choose decision Reject GOP
		Given user login to Pruhub
		And click menu	
		And go to workbasket menu
		And select Discharge task which "<Policy No>" Policy No which want to be decided
		When screen will be displayed with case detail at hospitalization tab
		Then go to decision and comment tab
		And choose "Reject GOP" decision
		And screen reject GOP will be displayed
		And search and select "fev" diagnose and input "tuberculosis" diagnosis remarks Reject GOP field
		And input "<Amount Decline>" amount decline and "<Actual LOS>" actual LOS Reject GOP field
		And input discharge date
		And input "<Decline Remarks>" decline remarks and "<Reason Decline Code>" reason decline code Reject GOP
		And click save
		And decision & comment tab will be displayed
		And write note : "<Note>"
		And click Submit to submit decision
		
		Examples:
		|Policy No	|Amount Decline	|Actual LOS	|Decline Remarks	|Reason Decline Code	|Note			|
		|ON6J0H 	|1000000		|2			|tolak				|02						|Reject GOP ->	|
		
	Scenario Outline: See Discharge task detail and choose decision Release LML
		Given user login to Pruhub
		And click menu	
		And go to workbasket menu
		And select Discharge task which "<Policy No>" Policy No which want to be decided
		When screen will be displayed with case detail at hospitalization tab
		Then go to decision and comment tab
		And choose "Release LML" decision
		And screen Release LML will be displayed
		And click add question
		And enter "<Question>" in question box
		And click save
		And decision & comment tab will be displayed
		And write note : "<Note>"
		And click Submit to submit decision
		
		Examples:
		|Question	|Note			|Policy No	|
		|How long?	|Release LML ->	|0VIPE3  	|
			